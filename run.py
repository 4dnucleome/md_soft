#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Maintainer: Michał Kadlof <michal.kadlof@mini.pw.edu.pl>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

import argparse
import configparser
import os
import sys
import time
from typing import List, Tuple, Any

import numpy as np
import openmm as mm
import points_io
from mdtraj.reporters import HDF5Reporter
from openmm import unit
from openmm.app import PDBFile, ForceField, Simulation, PDBReporter, DCDReporter, StateDataReporter
from openmm.unit import Quantity

from args_definition import ListOfArgs
from forces import add_harmonic_bond, add_constraints, add_harmonic_angle, add_excluded_volume, add_harmonic_restraints, add_spherical_container, add_andersen_thermostat, add_force_for_image_driven_correlational_method, add_force_for_image_driven_force_method
from md_utils import plot_data


def my_config_parser(config_parser: configparser.ConfigParser) -> List[Tuple[str, str]]:
    """Helper function that makes flat list arg name, and it's value from ConfigParser object."""
    sections = config_parser.sections()
    all_nested_fields = [dict(config_parser[s]) for s in sections]
    args_cp = []
    for section_fields in all_nested_fields:
        for name, value in section_fields.items():
            args_cp.append((name, value))
    return args_cp


def add_forces_to_system(system: mm.System, args: ListOfArgs):
    """Helper function, that add forces to the system."""
    if args.POL_USE_HARMONIC_BOND:
        add_harmonic_bond(system, args)
    elif args.POL_USE_CONSTRAINTS:
        add_constraints(system, args)
    if args.POL_USE_HARMONIC_ANGLE:
        add_harmonic_angle(system, args)
    if args.EV_USE_EXCLUDED_VOLUME:
        add_excluded_volume(system, args)
    if args.HR_USE_HARMONIC_RESTRAINTS:
        add_harmonic_restraints(system, args)
    if args.SC_USE_SPHERICAL_CONTAINER:
        add_spherical_container(system, args)
    if args.SIM_USE_ANDERSEN_THERMOSTAT:
        add_andersen_thermostat(system, args)

    if args.EF_RUN_IMAGE_DRIVEN:
        if args.EF_METHOD.lower() in ['force', 'f']:
            add_force_for_image_driven_force_method(system, args)
        elif args.EF_METHOD.lower() in ['correlation', 'corr', 'c']:
            add_force_for_image_driven_correlational_method(system, args)
        else:
            raise ValueError(f'Invalid value of EF_METHOD. Only [force|correlation] is allowed but got: {args.EF_METHOD}')


def get_integrator(random_seed: int, args: ListOfArgs) -> mm.Integrator:
    """Helper function that returns requested integrator."""
    integrator = mm.VerletIntegrator(10 * unit.femtosecond)  # default integrator
    if args.SIM_RUN_SIMULATION:
        if args.SIM_INTEGRATOR_TYPE == "langevin":
            integrator = mm.LangevinIntegrator(args.SIM_TEMP, args.SIM_FRICTION_COEFF, args.SIM_TIME_STEP)
            integrator.setRandomNumberSeed(random_seed)
        elif args.SIM_INTEGRATOR_TYPE == "brownian":
            print(args.SIM_TEMP, args.SIM_FRICTION_COEFF, args.SIM_TIME_STEP)
            integrator = mm.BrownianIntegrator(args.SIM_TEMP, args.SIM_FRICTION_COEFF, args.SIM_TIME_STEP)
            integrator.setRandomNumberSeed(random_seed)
        elif args.SIM_INTEGRATOR_TYPE == "verlet":
            integrator = mm.VerletIntegrator(args.SIM_TIME_STEP)
    return integrator


def get_config() -> ListOfArgs:
    """This function prepares the list of arguments.
    At first List of args with defaults is read.
    Then it's overwritten by args from config file (ini file).
    In the end config is overwritten by argparse options."""

    print(f"Reading config...")
    from args_definition import args
    arg_parser = argparse.ArgumentParser()

    arg_parser.add_argument('-c', '--config_file', help="Specify config file (ini format)", metavar="FILE")
    for arg in args:
        arg_parser.add_argument(f"--{arg.name.lower()}", help=arg.help)
    args_ap = arg_parser.parse_args()  # args from argparse
    config_parser = configparser.ConfigParser()
    config_parser.read(args_ap.config_file)
    args_cp = my_config_parser(config_parser)
    # Override defaults args with values from config file
    for cp_arg in args_cp:
        name, value = cp_arg
        arg = args.get_arg(name)
        arg.val = value
    # Now again override args with values from command line.
    for ap_arg in args_ap.__dict__:
        if ap_arg not in ['config_file']:
            name, value = ap_arg, getattr(args_ap, ap_arg)
            if value is not None:
                arg = args.get_arg(name)
                arg.val = value
    args.to_python()
    args.write_config_file()
    return args


def setup(args: ListOfArgs) -> Tuple[PDBFile, int, Simulation]:
    print("Initialization...")
    if args.SIM_RANDOM_SEED == 0:
        random_seed = np.random.randint(2147483647)
    else:
        random_seed = args.SIM_RANDOM_SEED
    print(f"   Loading initial structure: {args.INITIAL_STRUCTURE_PATH}")
    pdb = PDBFile(args.INITIAL_STRUCTURE_PATH)
    print(f"   Loading forcefield file:  {args.FORCEFIELD_PATH}")
    forcefield = ForceField(args.FORCEFIELD_PATH)
    print("   Building system...")
    system = forcefield.createSystem(pdb.topology)
    print("   Adding forces...")
    add_forces_to_system(system, args)
    print("   Integrator initialization...")
    integrator = get_integrator(random_seed, args)
    print("   Setting up simulation...")

    # Platform setting are here
    if args.platform:
        platform = mm.Platform.getPlatformByName(args.platform)
        if args.device:
            properties = {'DeviceIndex': args.device, 'Precision': 'double'}
            simulation = Simulation(pdb.topology, system, integrator, platform, properties)
        else:
            simulation = Simulation(pdb.topology, system, integrator, platform)
    else:
        simulation = Simulation(pdb.topology, system, integrator)
    current_platform = simulation.context.getPlatform()
    print(f"      Simulation will run on platform: {current_platform.getName()}")
    if "      DeviceIndex" in current_platform.getPropertyNames():
        print("      Device Id: ", current_platform.getPropertyValue(simulation.context, "DeviceIndex"))

    simulation.context.setPositions(pdb.positions)
    return pdb, random_seed, simulation


def minimize_energy(pdb: PDBFile, simulation: Simulation, args: ListOfArgs):
    if args.MINIMIZE:
        print('Energy minimizing...')
        simulation.minimizeEnergy()
        if not args.MINIMIZED_FILE:
            base, _ = os.path.splitext(args.INITIAL_STRUCTURE_PATH)
            minimized_file_name = f'{base}_min.pdb'
        else:
            minimized_file_name = args.MINIMIZED_FILE  # TODO: Nasty fix
        print(f'  Saving minimized structure in {minimized_file_name}')
        state = simulation.context.getState(getPositions=True)
        PDBFile.writeFile(pdb.topology, state.getPositions(), open(minimized_file_name, 'w'))


def run_md_simulation(random_seed, simulation, pdb, args):
    if args.SIM_RUN_SIMULATION:
        print("Running simulation...")
        if args.SIM_SET_INITIAL_VELOCITIES:
            print(f"   Setting up initial velocities at temperature {args.SIM_TEMP}")
            simulation.context.setVelocitiesToTemperature(args.SIM_TEMP, random_seed)
        reporting_to_screen_freq = max(1, int(round(args.SIM_N_STEPS / args.REP_STATE_N_SCREEN)))
        reporting_to_file_freq = max(1, int(round(args.SIM_N_STEPS / args.REP_STATE_N_FILE)))
        trajectory_freq = max(1, int(round(args.SIM_N_STEPS / args.TRJ_FRAMES)))

        total_time = args.SIM_N_STEPS * args.SIM_TIME_STEP
        print("   Number of steps:                 {} steps".format(args.SIM_N_STEPS))
        print("   Time step:                       {}".format(args.SIM_TIME_STEP))
        print("   Temperature:                     {}".format(args.SIM_TEMP))
        print("   Total simulation time:           {}".format(total_time.in_units_of(unit.nanoseconds)))
        print("   Number of state reads:           {} reads".format(args.REP_STATE_N_SCREEN))
        print("   State reporting to screen every: {} step".format(reporting_to_screen_freq))
        print("   State reporting to file every:   {} step".format(reporting_to_file_freq))
        print("   Number of trajectory frames:     {} frames".format(args.TRJ_FRAMES))
        print("   Trajectory frame every:          {} step".format(trajectory_freq))
        print("   Trajectory frame every:          {}".format(trajectory_freq * args.SIM_TIME_STEP))
        print('   Random seed:', random_seed)
        print()
        if args.TRJ_FILENAME_PDB:
            simulation.reporters.append(PDBReporter(args.TRJ_FILENAME_PDB, trajectory_freq))
        if args.TRJ_FILENAME_DCD:
            simulation.reporters.append(DCDReporter(args.TRJ_FILENAME_DCD, trajectory_freq))
        simulation.reporters.append(StateDataReporter(sys.stdout, reporting_to_screen_freq,
                                                      step=True, progress=True, potentialEnergy=True,
                                                      temperature=True, kineticEnergy=True, totalEnergy=True,
                                                      totalSteps=args.SIM_N_STEPS))
        if args.REP_STATE_FILE_PATH:
            simulation.reporters.append(StateDataReporter(args.REP_STATE_FILE_PATH, reporting_to_file_freq,
                                                          step=True, potentialEnergy=True,
                                                          temperature=True, kineticEnergy=True, totalEnergy=True))
        if args.REP_STATE_FILE_H5_PATH:
            simulation.reporters.append(HDF5Reporter(args.REP_STATE_FILE_H5_PATH, reporting_to_file_freq, velocities=True))

        print('Running simulation...')
        start = time.time()
        simulation.step(args.SIM_N_STEPS)
        end = time.time()
        elapsed = end - start
        speed = args.SIM_N_STEPS / elapsed
        print(f"\nFinished in {elapsed / 60:0.2f} minutes ({speed:0.1f} steps/s)\n")
        if args.TRJ_LAST_FRAME_PDB:
            last_frame_file_name = args.TRJ_LAST_FRAME_PDB
            state = simulation.context.getState(getPositions=True)
            PDBFile.writeFile(pdb.topology, state.getPositions(), open(last_frame_file_name, 'w'))
        if args.REP_PLOT_FILE_NAME:
            plot_data(args.REP_STATE_FILE_PATH, args.REP_PLOT_FILE_NAME)


def recreate_system_without_forces(args, state):
    if args.SIM_RANDOM_SEED == 0:
        random_seed = np.random.randint(2147483647)
    else:
        random_seed = args.SIM_RANDOM_SEED
    positions = np.array(state.getPositions(asNumpy=True))
    points_io.save_points_as_pdb(positions * 10, 'tmp.pdb', verbose=False)
    pdb = PDBFile('tmp.pdb', )
    forcefield = ForceField(args.FORCEFIELD_PATH)
    system = forcefield.createSystem(pdb.topology)
    integrator = get_integrator(random_seed, args)
    return system, pdb, integrator


def get_energy_components(simulation: mm.app.Simulation, args):
    forces = get_forces_parameters(simulation)
    real_total_energy = get_energy_from_state(simulation)
    state = simulation.context.getState(getPositions=True)

    energies = []
    for f in forces:
        system, pdb, integrator = recreate_system_without_forces(args, state)
        if f[1] == mm.openmm.HarmonicBondForce:
            bond_force = mm.HarmonicBondForce()
            system.addForce(bond_force)
            for bond in f[2]:
                i, j, dist, scale = bond
                bond_force.addBond(i, j, dist, scale)

            simulation = Simulation(pdb.topology, system, integrator)
            simulation.context.setPositions(pdb.positions)
            energy = get_energy_from_state(simulation)
            energies.append(("Harmonic bonds", energy))

        if f[1] == mm.openmm.HarmonicAngleForce:
            angle_force = mm.HarmonicAngleForce()
            system.addForce(angle_force)
            for angle in f[2]:
                i, j, k, ang, scale = angle
                angle_force.addAngle(i, j, k, ang, scale)

            simulation = Simulation(pdb.topology, system, integrator)
            simulation.context.setPositions(pdb.positions)
            energy = get_energy_from_state(simulation)
            energies.append(("Harmonic angles", energy))

        if f[1] == mm.openmm.CustomNonbondedForce:
            ev_force = mm.CustomNonbondedForce('epsilon*((sigma1+sigma2)/r)^12')
            ev_force.addGlobalParameter('epsilon', defaultValue=f[2][0])
            ev_force.addPerParticleParameter('sigma')
            system.addForce(ev_force)
            for i in range(simulation.system.getNumParticles()):
                ev_force.addParticle([args.EV_SIGMA])

            simulation = Simulation(pdb.topology, system, integrator)
            simulation.context.setPositions(pdb.positions)
            energy = get_energy_from_state(simulation)
            energies.append(("Custom non bonded", energy))

    s = 0.0 * unit.kilojoule / unit.mole
    print()
    print("Final frame potential energy components")
    for energy, value in energies:
        s += value
        print(f'{energy:18}: {value.format("%8.2f")} ({(value / real_total_energy * 100):0.2f}%)')
    other = real_total_energy - s
    print(f'{"Other":18}: {other.format("%8.2f")} ({(other / real_total_energy * 100):0.2f}%)')
    print("=" * 20)
    print(f'Total: {real_total_energy.format("%8.2f")}')


def get_energy_from_state(simulation: mm.app.Simulation) -> Quantity:
    state = simulation.context.getState(getPositions=True, getEnergy=True, getParameters=True)
    energy = state.getPotentialEnergy()
    return energy


def get_forces_parameters(simulation: mm.app.Simulation) -> List[Tuple[int, mm.openmm.Force, List[Any]]]:
    """Get the parameters of forces from simulation."""
    number_of_forces = simulation.system.getNumForces()
    forces = []
    for force_id in range(number_of_forces):
        force = simulation.system.getForce(force_id)
        force_type = type(force)
        if force_type == mm.openmm.HarmonicBondForce:
            number_of_bonds = force.getNumBonds()
            bonds_parameters = []
            for bond_id in range(number_of_bonds):
                bonds_parameters.append(force.getBondParameters(bond_id))
            forces.append((force_id, force_type, bonds_parameters))
        elif force_type == mm.openmm.HarmonicAngleForce:
            number_of_angles = force.getNumAngles()
            angles_parameters = []
            for angle_id in range(number_of_angles):
                angles_parameters.append(force.getAngleParameters(angle_id))
            forces.append((force_id, force_type, angles_parameters))
        elif force_type == mm.openmm.CustomNonbondedForce:
            global_parameter_number = force.getNumGlobalParameters()
            global_parameter_values = []
            for global_parameter_id in range(global_parameter_number):
                value = force.getGlobalParameterDefaultValue(global_parameter_id)
                global_parameter_values.append(value)
            forces.append((force_id, force_type, global_parameter_values))
    return forces


def main():
    print(f"Spring model - in house version.")
    print(f"OpenMM version: {mm.__version__}")
    args = get_config()
    pdb, random_seed, simulation = setup(args)
    minimize_energy(pdb, simulation, args)
    run_md_simulation(random_seed, simulation, pdb, args)
    get_energy_components(simulation, args)
    print()
    print("Everything is done")


if __name__ == '__main__':
    main()
