import os
from typing import Dict

import numpy as np
import openmm as mm
from openmm import AndersenThermostat
from scipy.stats import multivariate_normal

from args_definition import ListOfArgs
from helper_functions import standardize_image
from md_utils import sizeof_fmt


def add_harmonic_bond(system: mm.System, args: ListOfArgs):
    print("      Adding harmonic bonds...")
    print(f"         r0 = {args.POL_HARMONIC_BOND_R0}")
    print(f"         k = {args.POL_HARMONIC_BOND_K} kJ/mol/nm^2")
    bond_force = mm.HarmonicBondForce()
    system.addForce(bond_force)
    counter = 0
    for i in range(system.getNumParticles() - 1):
        bond_force.addBond(i, i + 1, args.POL_HARMONIC_BOND_R0, args.POL_HARMONIC_BOND_K)
        counter += 1
    print(f"         {counter} harmonic bonds added.")


def add_constraints(system: mm.System, args: ListOfArgs):
    print("      Adding constraints...")
    print(f"         r = {args.POL_CONSTRAINT_DISTANCE}")
    counter = 0
    for i in range(system.getNumParticles() - 1):
        system.addConstraint(i, i + 1, args.POL_CONSTRAINT_DISTANCE)
        counter += 1
    print(f"         {counter} constraints added.")


def add_harmonic_angle(system: mm.System, args: ListOfArgs):
    print("      Adding harmonic angles...")
    print(f"         r0 = {args.POL_HARMONIC_ANGLE_R0}")
    print(f"         k = {args.POL_HARMONIC_ANGLE_K} kJ/mol/radian^2")
    bond_force = mm.HarmonicAngleForce()
    system.addForce(bond_force)
    counter = 0
    for i in range(system.getNumParticles() - 2):
        bond_force.addAngle(i, i + 1, i + 2, args.POL_HARMONIC_ANGLE_R0, args.POL_HARMONIC_ANGLE_K)
        counter += 1
    print(f"         {counter} harmonic bonds added.")


def add_excluded_volume(system: mm.System, args: ListOfArgs):
    print("      Adding excluded volume...")
    print(f"         epsilon = {args.EV_EPSILON}")
    print(f"         sigma = {args.EV_SIGMA}")
    ev_force = mm.CustomNonbondedForce('epsilon*((sigma1+sigma2)/r)^12')
    ev_force.addGlobalParameter('epsilon', defaultValue=args.EV_EPSILON)
    ev_force.addPerParticleParameter('sigma')
    system.addForce(ev_force)
    counter = 0
    for i in range(system.getNumParticles()):
        ev_force.addParticle([args.EV_SIGMA])
        counter += 1
    print(f"         {counter} ev interactions added.")


def add_harmonic_restraints(system: mm.System, args: ListOfArgs):
    """Restraints format is different here than in SM webservice.
    Example record: :10 :151\n
    or
    :10 :151 0.1 30000\n
    :i :j distance energy
    """
    print("      Adding harmonic restraints")
    if args.HR_USE_FLAT_BOTTOM_FORCE:
        contact_force = mm.CustomBondForce('step(r-r0) * (k/2) * (r-r0)^2')
        contact_force.addPerBondParameter('r0')
        contact_force.addPerBondParameter('k')
    else:
        contact_force = mm.HarmonicBondForce()
    system.addForce(contact_force)

    with open(args.HR_RESTRAINTS_PATH) as input_file:
        counter = 0
        for line in input_file:
            columns = line.split()
            atom_index_i = int(columns[0][1:]) - 1
            atom_index_j = int(columns[1][1:]) - 1
            try:
                r0 = float(columns[2])
                k = float(columns[3]) * args.HR_K_SCALE
            except IndexError:
                r0 = args.HR_R0_PARAM
                k = args.HR_K_PARAM
            if args.HR_USE_FLAT_BOTTOM_FORCE:
                contact_force.addBond(atom_index_i, atom_index_j, [r0, k])
            else:
                contact_force.addBond(atom_index_i, atom_index_j, r0, k)
            counter += 1
    print(f"         {counter} restraints added.")


def add_spherical_container(system: mm.System, args: ListOfArgs):
    print("      Adding spherical container...")
    container_force = mm.CustomExternalForce(
        '{}*max(0, r-{})^2; r=sqrt((x-{})^2+(y-{})^2+(z-{})^2)'.format(args.SC_SCALE,
                                                                       args.SC_RADIUS,
                                                                       args.SC_CENTER_X,
                                                                       args.SC_CENTER_Y,
                                                                       args.SC_CENTER_Z,
                                                                       ))
    system.addForce(container_force)
    for i in range(system.getNumParticles()):
        container_force.addParticle(i, [])
    print(f"         Spherical container added.")
    print(f"            radius: {args.SC_RADIUS} nm")
    print(f"            scale:  {args.SC_SCALE} ")
    print(f"            center: ({args.SC_CENTER_X}, {args.SC_CENTER_Y}, {args.SC_CENTER_Z})")


def add_andersen_thermostat(system, args):
    print("      Adding Andersen thermostat...")
    collision_frequency = args.SIM_ANDERSEN_THERMOSTAT_COLLISION_FREQ_NOMINATOR / args.SIM_ANDERSEN_THERMOSTAT_COLLISION_FREQ_NOMINATOR
    system.addForce(AndersenThermostat(args.SIM_TEMP, collision_frequency))
    print(f"         Andersen thermostat added.")
    print(f"            Temperature: {args.SIM_TEMP} nm")
    print(f"            Collision Frequency: {collision_frequency} ")


def add_external_field(args: ListOfArgs) -> Dict:
    """Add external forcefield for image-driven modelling purposes."""
    print('      Adding external forcefield.')
    size = os.stat(args.EF_PATH).st_size
    print(f"   Reading {args.EF_PATH} file ({sizeof_fmt(size)})...")
    img = np.load(args.EF_PATH).astype(float)
    print(f"   Array of shape {img.shape} loaded.")
    print(f"   Number of values: {img.size}")
    print(f"   Min: {np.min(img)}")
    print(f"   Max: {np.max(img)}")
    if args.EF_NORMALIZE:
        print('   [INFO] Field will be normalized to [0, -1]')
        img = standardize_image(img)
    print(f'   [INFO] IMG min = {np.min(img)}, max = {np.max(img)}')
    print("  Creating a force based on density...")
    voxel_size = np.array((args.EF_VOXEL_SIZE_X, args.EF_VOXEL_SIZE_Y, args.EF_VOXEL_SIZE_Z))
    real_size = img.shape * voxel_size
    if args.EF_IMAGE_ORIENTATION.lower() == 'xyz':
        x_size = img.shape[0],
        y_size = img.shape[1],
        z_size = img.shape[2],
    elif args.EF_IMAGE_ORIENTATION.lower() == 'zyx':
        x_size = img.shape[2],
        y_size = img.shape[1],
        z_size = img.shape[0],
    else:
        raise ValueError(f"Invalid value of EF_IMAGE_ORIENTATION. Allowed values are only 'xyz' or 'zyx' but got: {args.EF_IMAGE_ORIENTATION}")
    density_fun_args = dict(
        xsize=x_size[0],
        ysize=y_size[0],
        zsize=z_size[0],
        values=img.flatten(order=args.EF_NUMPY_FLATTEN_ORDER).astype(np.float64),
        xmin=0 * mm.unit.angstrom,
        ymin=0 * mm.unit.angstrom,
        zmin=0 * mm.unit.angstrom,
        xmax=(img.shape[0] - 1) * voxel_size[0],
        ymax=(img.shape[1] - 1) * voxel_size[1],
        zmax=(img.shape[2] - 1) * voxel_size[2])
    print(f'   [INFO] Voxel size: ({args.EF_VOXEL_SIZE_X}, {args.EF_VOXEL_SIZE_Y}, {args.EF_VOXEL_SIZE_Z})')
    print(f'   [INFO] Real size (Shape * voxel size): ({real_size[0]}, {real_size[1]}, {real_size[2]})')
    print(
        f"   [INFO] begin coords: ({density_fun_args['xmin']}, {density_fun_args['ymin']}, {density_fun_args['zmin']})")
    print(
        f"   [INFO] end coords:   ({density_fun_args['xmax']}, {density_fun_args['ymax']}, {density_fun_args['zmax']})")
    center_x = (density_fun_args['xmax'] - density_fun_args['xmin']) / 2 + density_fun_args['xmin']
    center_y = (density_fun_args['ymax'] - density_fun_args['ymin']) / 2 + density_fun_args['ymin']
    center_z = (density_fun_args['zmax'] - density_fun_args['zmin']) / 2 + density_fun_args['zmin']
    print(f"   [INFO] Image central point: ({center_x}, {center_y}, {center_z}) ")
    return density_fun_args


def add_force_for_image_driven_force_method(system: mm.System, args: ListOfArgs):
    """Image driven force method"""
    density_fun_args = add_external_field(args)
    field_function = mm.Continuous3DFunction(**density_fun_args)
    field_force = mm.CustomCompoundBondForce(1, 'ksi*fi(x1,y1,z1)')
    field_force.addTabulatedFunction('fi', field_function)
    field_force.addGlobalParameter('ksi', args.EF_SCALING_FACTOR)
    print("  Adding force to the system...")

    for i in range(system.getNumParticles()):
        field_force.addBond([i], [])
    system.addForce(field_force)


def add_force_for_image_driven_correlational_method(system: mm.System, args: ListOfArgs):
    """Image driven correlational method"""
    density_fun_args = add_external_field(args)

    # normal distribution field
    norm_size = 20
    var = 3
    res = 5
    norm_field = np.zeros((norm_size, norm_size, norm_size))
    rv = multivariate_normal([0] * 3, var * np.eye(3))
    for i in range(norm_size):
        for j in range(norm_size):
            for k in range(norm_size):
                norm_field[i][j][k] = rv.pdf([i / res, j / res, k / res])
    norm_fun_args = dict(
        xsize=norm_size,
        ysize=norm_size,
        zsize=norm_size,
        values=norm_field.flatten().astype(np.float64),
        xmin=-0.5 / res,
        ymin=-0.5 / res,
        zmin=-0.5 / res,
        xmax=(norm_size - 0.5) / res,
        ymax=(norm_size - 0.5) / res,
        zmax=(norm_size - 0.5) / res)

    field_function = mm.Continuous3DFunction(**density_fun_args)
    norm_distribution_function = mm.Continuous3DFunction(**norm_fun_args)
    n = system.getNumParticles()

    xmin, xmax = density_fun_args["xmin"]._value, density_fun_args["xmax"]._value
    ymin, ymax = density_fun_args["ymin"]._value, density_fun_args["ymax"]._value
    zmin, zmax = density_fun_args["zmin"]._value, density_fun_args["zmax"]._value

    v = 0.2
    b = 7
    range_x = np.linspace(xmin + v, xmax - v, b)
    range_y = np.linspace(ymin + v, ymax - v, b)
    # range_z = [0.06, 0.12, 0.18]  # [0.12]  # np.linspace(zmin+v, zmax-v, b)
    range_z = [0.12]
    print(xmin, xmax, ymin, ymax, zmin, zmax)

    numerator = []
    denominator = []
    for xi in range_x:
        for yi in range_y:
            for zi in range_z:
                field_val = 'd(' + str(xi) + ',' + str(yi) + ',' + str(zi) + ')'
                numerator_norm_sum = []
                for i in range(n):
                    if i % 3 == 0:
                        numerator_norm_sum.append('dn(abs(x' + str(i + 1) + '-' + str(xi)
                                                  + '), abs(y' + str(i + 1) + '-' + str(yi)
                                                  + '), abs(z' + str(i + 1) + '-' + str(zi) + '))')

                numerator.append(field_val + '*(' + '+'.join(numerator_norm_sum) + ')')
                denominator.append('(' + '+'.join(numerator_norm_sum) + ')^2')
    numerator = '(' + '+'.join(numerator) + ')'
    denominator = 'sqrt(' + '+'.join(denominator) + ')'
    force_def = 'ksi*' + numerator + '/' + denominator

    field_force = mm.CustomCompoundBondForce(n, force_def)

    field_force.addTabulatedFunction('d', field_function)
    field_force.addTabulatedFunction('dn', norm_distribution_function)
    field_force.addGlobalParameter('ksi', args.EF_SCALING_FACTOR)

    print("  Adding force to the system...")
    field_force.addBond(list(range(n)), [])
    system.addForce(field_force)
