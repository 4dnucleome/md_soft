### Description ###

Ecosystem for performing several types of MD calculations for polymer physics particularly dedicated fo chromatin.
It uses [openMM](http://openmm.org/) as modelling engine. 

### Requirements ###

This version was tested with:
 * python 3.10.12
 * [OpenMM 8.0.0-3b3def0](https://github.com/openmm/openmm/tree/3b3def0e3efe0bd76f2754b0b65417d6c9cc5e70)

Optionally you may wish to install CUDA or OpenCL libraries for GPU support, which is highly recommended.

### Setup ###

Recommended way to set up md-soft is to use [pyenv](https://github.com/pyenv/pyenv-installer), for setting up separate
virtual environment.

You need to install openMM - either compile openMM from
sources: [How-to](http://docs.openmm.org/latest/userguide/library.html#compiling-openmm-from-source-code) or use
conda [Installing OpenMM](http://docs.openmm.org/latest/userguide/application/01_getting_started.html#installing-openmm).

Clone repo:

    git clone git@bitbucket.org:4dnucleome/md_soft.git

Install dependencies:

    pip install -r requirements.txt

### Running simulations

Assuming your config file is named `config.ini`

    python run.py -c config.ini

### Choosing platform (GPU) to run on

Check available platforms:

    python -m openmm.testInstallation

OpenMM selects the fastest platform to run on automatically. To override the default you can use:

Run on CPU:

    python run.py -c config.ini --platform CPU

Run on CUDA:

    python run.py -c config.ini --platform CUDA

Run on CUDA on device 0:

    python run.py -c config.ini --platform CUDA --device 0

Run on OpenCL:

    python run.py -c config.ini --platform OpenCL

### Choosing platform (GPU) to run on

Check available platforms:

    python -m openmm.testInstallation

OpenMM selects the fastest platform to run on automatically. To override the default you can use:

Run on CPU:

    run.py -c config.ini --platform CPU

Run on CUDA:

    run.py -c config.ini --platform CUDA

Run on CUDA on device 0:

    run.py -c config.ini --platform CUDA --device 0

Run on OpenCL:

    run.py -c config.ini --platform OpenCL

### Other topics

##### Preparing initial structure

Your simulation need initial structure. You may find useful scripts for generating initial structures
in [this repository](https://bitbucket.org/mkadlof/structuregenerator).

##### Image driven

First step of using image driven modeling is creating the initial structure for the task. For that, we will be
using [structuregenerator](https://bitbucket.org/4dnucleome/structuregenerator/src/master/). The installation is very
straightforward:

    pip install -U pip
    pip install numpy Cython
    pip install git+https://bitbucket.org/4dnucleome/structuregenerator.git

Then, you can generate initial structure of your choice, e.g. by using:

    generator img_is 200 examples/example_idm/27-7x7_z50_2_nuc35-syg2.cmap

That generates initial_structure.pdb that will be used for the further simulations.

The next step is running the simulations with the provided config_id.ini config. All the parameters are explained, so
you can configure them properly. Be sure to set properly voxel sizes, as they need to be consistent between OpenMM and
Chimera.

After that, you can simply run the simulation:

    python run.py -c config_id.ini

##### Parameters

The parameters can be set either in the config.ini file, or in the command line. The list of parameters with their
descriptions is provided below. To use them in CLI, you need to just change the uppercase to lowercase (e.g. from
PLATFORM to --platform).

| Parameter                  | Definition                                                                                                                                 | Default           |
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|-------------------|
| PLATFORM                   | name of the platform. Available choices: Reference CPU, CUDA                                                                               |                   | 
| DEVICE                     | device index for CUDA or OpenCL (count from 0)                                                                                             |                   |
| INITIAL_STRUCTURE_PATH     | Path to PDB file.                                                                                                                          |                   |
| FORCEFIELD_PATH            | Path to XML file with forcefield.                                                                                                          |                   |
| POL_USE_HARMONIC_BOND      | Use harmonic bond interaction.                                                                                                             | True              |
| POL_HARMONIC_BOND_R0       | harmonic bond distance equilibrium constant                                                                                                | 0.1 nanometer     |
| POL_HARMONIC_BOND_K        | harmonic bond force constant (fixed unit: kJ/mol/nm^2)                                                                                     | 300000.0          |
| POL_USE_CONSTRAINTS        | Use fixed bond length instead of harmonic interaction                                                                                      | False             |
| POL_CONSTRAINT_DISTANCE    | Fixed constraints length                                                                                                                   | 0.1 nanometer     |
| POL_USE_HARMONIC_ANGLE     | Use harmonic angle interaction.                                                                                                            | True              |
| POL_HARMONIC_ANGLE_R0      | harmonic angle distance equilibrium constant                                                                                               | 3.141592653589793 |
| POL_HARMONIC_ANGLE_K       | harmonic angle force constant (fixed unit: kJ/mol/radian^2)                                                                                | 10.0              |
| EV_USE_EXCLUDED_VOLUME     | Use excluded volume.                                                                                                                       | False             |
| EV_EPSILON                 | Epsilon parameter.                                                                                                                         | 2.86              |
| EV_SIGMA                   | Sigma parameter.                                                                                                                           | 0.05 nanometer    |
| HR_USE_HARMONIC_RESTRAINTS | Use long range interactions or not                                                                                                         | False             |
| HR_USE_FLAT_BOTTOM_FORCE   | Use flat bottom force instead of standard harmonic force.                                                                                  | False             |
| HR_RESTRAINTS_PATH         | Path to .rst file with indices                                                                                                             |                   |
| HR_R0_PARAM                | distance constant, this value will be used only if it is missing in rst file                                                               |                   |
| HR_K_PARAM                 | force constant, this value will be used only if it is missing in rst file (Fixed unit: kilojoule_per_mole/nanometer**2 - only float number | needed.)          |
| HR_K_SCALE                 | force constant scaling factor applied if you are using values from rst file.                                                               | 1                 |                 
| SC_USE_SPHERICAL_CONTAINER | Use Spherical container                                                                                                                    | False             |             
| SC_CENTER_X                | Spherical container location x, fixed unit: nanometers                                                                                     |                   |
| SC_CENTER_Y                | Spherical container location y, fixed unit: nanometers                                                                                     |                   |
| SC_CENTER_Z                | Spherical container location z, fixed unit: nanometers                                                                                     |                   |
| SC_RADIUS                  | Spherical container radius, fixed unit: nanometers                                                                                         | 30                |                
| SC_SCALE                   | Spherical container scaling factor                                                                                                         | 1000              |              
| MINIMIZE                   | should initial structure be minimized? - This is spring model main functionality.                                                          | True              |              
| MINIMIZED_FILE             | If left empty result file will have name based on initial structure file name with _min.pdb ending.                                        |                   |
| SIM_RUN_SIMULATION         | Do you want to run MD simulation?                                                                                                          | False             |             
| SIM_INTEGRATOR_TYPE        | Alternative: langevin, verlet                                                                                                              | verlet            |            
| SIM_FRICTION_COEFF         | Friction coefficient (Used only with langevin integrator)                                                                                  |                   |
| SIM_N_STEPS                | Number of steps in MD simulation                                                                                                           |                   |
| SIM_TIME_STEP              | Time step (use time unit from simtk.unit module)                                                                                           |                   |
| SIM_TEMP                   | Temperature (use temperature unit from simtk.unit module)                                                                                  |                   |
| SIM_RANDOM_SEED            | Random seed. Set to 0 for random seed.                                                                                                     | 0                 |                 
| SIM_SET_INITIAL_VELOCITIES | Sets initial velocities based on Boltzmann distribution                                                                                    | False             |             
| TRJ_FRAMES                 | Number of trajectory frames to save.                                                                                                       | 2000              |              
| TRJ_FILENAME_DCD           | Write trajectory in DCD file format, leave empty if you do not want to save.                                                               |                   |
| TRJ_FILENAME_PDB           | Write trajectory in PDB file format, leave empty if you do not want to save.                                                               |                   |
| TRJ_LAST_FRAME_PDB         | Write last frame of trajectory in PDB file format, leave empty if you do not want to save.                                                 |                   |
| REP_STATE_N_SCREEN         | Number of states reported on screen                                                                                                        | 20                |                
| REP_STATE_N_FILE           | Number of states reported to file screen                                                                                                   | 1000              |              
| REP_STATE_FILE_PATH        | Filepath to save state. Leave empty if not needed.                                                                                         | state.csv         |         
| REP_STATE_FILE_H5_PATH     | H5 file to save velocities. Leave empty if not needed.                                                                                     | state.h5          |          
| REP_PLOT_FILE_NAME         | Filepath to save energy plot. Leave empty if not needed.                                                                                   | energy.pdf        |        
| EF_USE_EXTERNAL_FIELD      | External force                                                                                                                             | False             |             
| EF_PATH                    | npy file, that defines regular 3D grid with external field values                                                                          |                   |
| EF_VOXEL_SIZE_X            | External Field Voxel size X                                                                                                                |                   |
| EF_VOXEL_SIZE_Y            | External Field Voxel size Y                                                                                                                |                   |
| EF_VOXEL_SIZE_Z            | External Field Voxel size Z                                                                                                                |                   |
| EF_NORMALIZE               | Should the field be normalized to [0;1]?                                                                                                   | False             |             
| EF_SCALING_FACTOR          | External field scaling factor                                                                                                              | 1.0               |               

##### General tips

Distance Constraints on consecutive beads doesn't work with harmonic flat angle. Use harmonic bond instead.

#### Contact

Michał Kadlof <m.kadlof@mini.pw.pl>
