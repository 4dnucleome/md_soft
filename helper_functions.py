from scipy import ndimage


def add_funnel(img, mask_n):
    funnel = ndimage.distance_transform_edt(mask_n)
    funnel = (funnel - funnel.min(initial=None)) / (funnel.max(initial=None) - funnel.min(initial=None)) * 1
    return img + funnel


def standardize_image(img):
    """returns image (0, -1)"""
    return - (img - img.min()) / (img.max() - img.min())
