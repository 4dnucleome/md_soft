This is an example of running a basic image-driven modeling for a prepared artificial test system.

1. Explore the system:

1.1 View the visualization of the artificial image: `image.png`
1.2 View the visualization of the potential function derived from the image: `potential-2d.png`
1.3 Open the system in UCSF Chimera:
    a) The easy way: open the saved Chimera session: `chimera_session-modelling-scene.py`
    b) The hard way:
       - Open the `image.cmap` file
       - Open the `coordinates-for-chimera.bld` file
       and adjust the display according to your needs.

2. Run the simulation:

2.1 Make sure you are in the folder where the config.ini file is located.
2.2 Run the simulation with the following command:

    ../../run.py -c config.ini

2.3 Wait for approximately 1 minute until the simulation finishes. The execution time may vary.

3. Explore the results:

3.1 Open the energy plot `results/plot.png`
3.2 Open the system the same way as in step 1.3.
3.3 Display the trajectory by:
- Go to Tools -> MD/Ensemble Analysis -> MD Movie
- Select NAMD (PSF/DCD)
- Choose the `structure.psf` file with the topology
- Adding the trajectory file `results/trj.dcd`
3.4 Click play and enjoy :)

